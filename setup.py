from setuptools import setup

setup(
    name='OSU-Competitive-Esports-Bot',
    version='1.2.1',
    url='https://gitlab.com/osu-competetive-esports/OSU-Competitive-Esports-Discord-Bot',
    license='GNU AGPLv3',
    author='Alex Shafer',
    author_email='enzanki.ars@gmail.com',
    description='OSU Competitive Esports Discord Bot',
    install_requires=['asyncio >= 3.4, < 3.5',
                      'google-api-python-client >= 1.7, < 1.8',
                      'oauth2client >= 4.1, < 4.2',
                      'discord.py >= 1.3, < 1.4'
                      'requests >= 2.20, < 2.21']
)
